import React from 'react';
import ReactDOM from 'react-dom';
import getCurrentDateParam from './Date';

describe('Date helper', () => {
  it('should return current date in format YYYYMMDD', () => {
    expect(getCurrentDateParam(new Date('December 9, 2019'))).toBe('20191209');
    expect(getCurrentDateParam(new Date('December 17, 1995'))).toBe('19951217');
  });
})