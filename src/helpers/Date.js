const getCurrentDateParam = (vDate) => {
  return `${vDate.getFullYear()}${vDate.getMonth()+1 <= 9 ? '0' + vDate.getMonth()+1 : vDate.getMonth()+1}${vDate.getDate() <= 9 ? '0' + vDate.getDate() : vDate.getDate()}`;
}

export default getCurrentDateParam;