import React from 'react';
import ReactDOM from 'react-dom';
import LocationList from './LocationList';

describe('LocationList tests', () => {
  let mountedLocationList;
  beforeEach(() => {
    mountedLocationList = document.createElement('div');
  })

  it('renders without crashing', () => {
    ReactDOM.render(<LocationList />, mountedLocationList);
    ReactDOM.unmountComponentAtNode(mountedLocationList);
  });
})