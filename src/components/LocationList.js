import React from 'react';
import PropTypes from 'prop-types';
import '../css/location-list.css';

class LocationList extends React.Component {
  render() {
    return(
      <div className="location-list">
        <ul className="list-group">
          {this.props.venues.map( venue => ( 
            <li 
              className="list-group-item"
              key={venue.venue.id}
            >
              <span>{venue.venue.name}</span>
              <span className="address">{venue.venue.location.address}</span>
              <span>{venue.venue.location.address.city}</span>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}
LocationList.propTypes = {
  venues: PropTypes.arrayOf(PropTypes.shape({}))
}

LocationList.defaultProps = {
  venues: []
}

export default LocationList;