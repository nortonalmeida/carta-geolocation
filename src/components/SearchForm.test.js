import React from 'react';
import ReactDOM from 'react-dom';
import SearchForm from './SearchForm';

describe('SearchForm tests', () => {
  const mockGeolocation = {
    getCurrentPosition: jest.fn()
  };
  
  global.navigator.geolocation = mockGeolocation;
  let mountedSearchForm;
  beforeEach(() => {
    mountedSearchForm = document.createElement('div');
  })

  it('renders without crashing', () => {
    ReactDOM.render(<SearchForm />, mountedSearchForm);
    ReactDOM.unmountComponentAtNode(mountedSearchForm);
  });

  it('expected getCurrentLocation to have been called', () => {
    expect(mockGeolocation.getCurrentPosition).toHaveBeenCalledTimes(1);
  });

  
})