import React from 'react';
import keys from '../config/keys';
import axios from 'axios';
import LocationList from './LocationList';
import getCurrentDateParam from '../helpers/Date';
import '../css/search-form.css';

class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      searchTerm: '',
      venues: []
    }
  }

  componentDidMount() {
    this.getCurrentLocation();
  }

  updateSearchTerm = (event) => {
    event.preventDefault();
    this.setState({
      searchTerm: event.target.value
    });
    this.getVenues(this.state.searchTerm)
  }

  getCurrentLocation() {
    navigator.geolocation.getCurrentPosition( response => {
      const position = response.coords;
      this.setState({
        location: `${position.latitude},${position.longitude}`
      }, () => {
        this.getVenues(this.state.searchTerm);
      });
    });
  }

  getVenues(query) {
    const url = keys.API_URL;
    const params = {
      client_id: `${keys.CLIENT_ID}`,
      client_secret: `${keys.CLIENT_SECRET}`,
      ll: this.state.location,
      v: getCurrentDateParam(new Date()),
      limit: 10,
      query: this.state.searchTerm
    };
    axios.get(`${url + new URLSearchParams(params)}`)
      .then( response => {
        this.setState({ venues: response.data.response.groups[0].items });
      })
      .catch( error => {
        console.log("ERROR:", error);
      })
  }

  render() {
    return(
      <div>
        <form className="search-form" onSubmit={this.getVenues}>
          <div className="form-group">
            <div className="col-sm-12 col-md-12 col-lg-12">
              <input 
                type="text"
                className="form-control"
                placeholder="input location name (auto search)"
                onChange={this.updateSearchTerm} 
              />
            </div>
          </div>
        </form>
        <LocationList {...this.state} />
      </div>
    )
  }
}

export default SearchForm;