import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

describe('App.js Geolocation methods', () => {
  const mockGeolocation = {
    getCurrentPosition: jest.fn()
  };
  
  global.navigator.geolocation = mockGeolocation;

  let mountedApp;
  beforeEach(() => {
    mountedApp = document.createElement('div');
  });

  it('renders without crashing', () => {
    ReactDOM.render(<App />, mountedApp);
    ReactDOM.unmountComponentAtNode(mountedApp);
  });

  it('expected getCurrentLocation to have been called', () => {
    expect(mockGeolocation.getCurrentPosition).toHaveBeenCalledTimes(1);
  });
})