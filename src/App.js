import React from 'react';
import SearchForm from './components/SearchForm';
import './css/App.css';

class App extends React.Component {
  state = {
    venues: []
  }
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md">
            <SearchForm />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
